1. In our hotel database, create a "rooms" collection and insert a single room with the following details:
name - single
accomodates - 2
price - 1000
description - A simple room with all the basic necessities
rooms_available - 10
isAvailable - false

db.hotel.insertOne(
	{
		"name" : "single",
	    "accommodates" : 2,
	    "price" : 1000, 
	    "description" : "A simple room with all the basic necessities",
	    "rooms_available" : 10, 
	    "isAvailable" : false
	}
)


2. Insert multiple rooms with the following details
name - double
accomodates - 3
price - 2000
description - A room fit for a small family going on a vacation
rooms_available - 5
isAvailable - false

name - queen
accomodates - 4
price - 4000
description - A room with a queen sized bed perfect for a simple getaway
rooms_available - 15
isAvailable - false 

db.hotel.insertMany(
	{
		"name": "double",
	    "accommodates": 3,
	    "price": 2000, 
	    "description" : "A room fit for a small family going on a vacation",
	    "rooms_available" : 5, 
	    "isAvailable" : false
	},
	{
		"name": "queen",
	    "accommodates": 4,
	    "price": 4000, 
	    "description" : "A room with a queen sized bed perfect for a simple getaway",
	    "rooms_available" : 15, 
	    "isAvailable" : false
	}
)

3. Find rooms with a price greater than or equal to 2000

db.hotels.find(
	{
		"price": {
			$gte: 2000
		},
) 
4. Find a room with a price greater than or equal to 2000 and with rooms_available greater than 10

db.hotels.find(
	{
		"price": {
			$gte: 2000
		},
		"rooms_available": {
			$gt: 10
		},
5. Update the "queen" room to have rooms_available to 0

6. Find all rooms with rooms_available greater than 0 and the isAvailable to false and set the availability of the rooms to "true"

7. Delete all rooms with rooms_available equal to 0

8. Query a room with the name "double" and project only the following fields:

9. Do the same query as previously done but exclude the id.
https://gitlab.com/tuitt/students/batch126/resources/s23/d1/-/blob/master/mongo.js

Updated 8 and 9:
8. Query a room with the name "double" and project only the following fields:

name
accomodates
price
description

9. Do the same query as previously done but exclude the id.